'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

//Route.resource('usuarios', 'UsuarioController')
  //.apiOnly().middleware(["auth"])

Route.get('usuarios','UsuarioController.index').middleware(["auth"])

Route.get('usuarios/:id','UsuarioController.show').middleware(["auth"])

Route.delete('usuarios/:id','UsuarioController.destroy').middleware(["auth"])

Route.post('usuarios','UsuarioController.store')

Route.put('usuarios/:id','UsuarioController.update').middleware(["auth"])

Route.resource('pedidos', 'PedidoController')
  .apiOnly()

Route.resource('itens', 'ItemController')
  .apiOnly()

Route.resource('pratos', 'PratoController')
  .apiOnly()

Route.resource('tipospratos', 'TipoPratoController')
  .apiOnly()

Route.resource('tiposprodutos', 'TipoProdutoController')
  .apiOnly()

Route.get('pdf','UsuarioController.generatePdf')

Route.post('/authenticate','UsuarioController.authenticate')