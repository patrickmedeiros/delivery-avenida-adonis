'use strict'

const Usuario = use('App/Models/User')
const Database = use('Database')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with usuarios
 */
class UsuarioController {
  
  async index () {
    const usuarios = Usuario.all()

    return usuarios
  }
  

  async store ({ request }) {
    const data = request.only([
      'username',
      'email',
      'password',
      'admin',
      'cpf'
    ])

    const usuario = await Usuario.create(data)

    return usuario
  }


  async show ({ params }) {
    const usuario = await Usuario.findOrFail(params.id)

    return usuario
  }

  
  async update ({ params, request }) {
    const usuario = await Usuario.findOrFail(params.id)

    const data = request.only([
      'username',
      'email',
      'password',
      'admin',
      'cpf'
    ])

    usuario.merge(data)

    await usuario.save()
    const email = usuario.email
    const username = usuario.username

    const response = {email, username}
    return response
  }

  
  async destroy ({ params, response }) {
    const usuario = await Usuario.find(params.id)

    //verificando se usuário existe ou não para ser deletado
    if (usuario != null){
      await usuario.delete()
      return response.send({ok: 'Usuário deletado com sucesso'})
    } else{
      return response.send({error: 'Usuário não encontrado'})
    }
    
    /*if (usuario != null) {
      return response.status(200).send({ error: 'Usuário deletado com sucesso' })
    }else{
      return response.status(404).send({ error: 'Usuário não encontrado' })

    }*/
  }

  /*async generatePdf({ response }){
    const dados = []

    //const string = await Database.table('users').where({id: 1})
    
    var i

    for(i = 1; i <= 3; i ++){
      const string = await Database.table('pratoes').where({id: i})
    
      const obj = JSON.stringify(string)


      const nome = obj.split(",")

      //const nome = kkk.substring(2)

      dados.push(nome[1])
      
    }

    const content = [{ text: dados}]

    pdf = PDF.create(content, response.response)
      
    return pdf
  }*/

  async generatePdf({ request }){
    
    const { username } = request.all()

    const dados = []

    dados.push(username)

    const content = [{ text: dados}]

    pdf = PDF.create(content, response.response)
      
    return pdf
  }

  async authenticate({request, auth, params}){
    //precisa de id, nome e e-mail
    const {email, password} = request.all()

    const accesstoken = await auth.attempt(email, password) 
   
    const query = await Database.select('id', 'username', 'email','admin').from('users').where('email', email).first()

    //const usuario = await Usuario.findOrFail(params.email)
    
    const data = { query, accesstoken}
    
    return data
  }

}

module.exports = UsuarioController