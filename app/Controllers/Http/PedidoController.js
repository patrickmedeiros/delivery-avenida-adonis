'use strict'

const Pedido = use('App/Models/Pedido')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pedidos
 */
class PedidoController {
  async index () {
    const pedidos = Pedido.all()

    return pedidos
  }
  

  async store ({ request }) {
    const data = request.only([
      'valor',
      'pago',
      'user_id',
      'prato_id'
    ])

    const pedido = await Pedido.create(data)

    return pedido
  }


  async show ({ params }) {
    const pedido = await Pedido.findOrFail(params.id)

    return pedido
  }

  
  async update ({ params, request}) {
    const pedido = await Pedido.findOrFail(params.id)

    const data = request.only([
      'valor',
      'pago',
      'user_id',
      'prato_id'
    ])

    pedido.merge(data)

    await pedido.save()

    return pedido
  }

  
  async destroy ({ params, response }) {
    const pedido = await Pedido.find(params.id)

    if (pedido != null){
      await pedido.delete()
      return response.send({ok: 'Pedido deletado com sucesso'})
    } else{
      return response.send({error: 'Pedido não encontrado'})
    }
  }
}

module.exports = PedidoController
