'use strict'

const Item = use('App/Models/Item')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with items
 */
class ItemController {
  
  async index () {
    const itens = Item.all()

    return itens
  }
  

  async store ({ request }) {
    const data = request.only([
      'nome',
      'quantidade',
      'preco',
      'descricao',
      'tipo_produto_id'
    ])

    const item = await Item.create(data)

    return item
  }


  async show ({ params }) {
    const item = await Item.findOrFail(params.id)

    return item
  }

  
  async update ({ params, request }) {
    const item = await Item.findOrFail(params.id)

    const data = request.only([
      'nome',
      'quantidade',
      'preco',
      'descricao',
      'tipo_produto_id'
    ])

    item.merge(data)

    await item.save()

    return item
  }

  
  async destroy ({ params, response }) {
    const item = await Item.find(params.id)

    if (item != null){
      await item.delete()
      return response.send({ok: 'Item deletado com sucesso'})
    } else{
      return response.send({error: 'Item não encontrado'})
    }
  }
}

module.exports = ItemController
