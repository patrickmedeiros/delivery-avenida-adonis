'use strict'

const TipoProduto = use('App/Models/TipoProduto')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class TipoProdutoController {
    async index () {
        const tiposProdutos = await TipoProduto.query().with('item').fetch();
    
        return tiposProdutos
      }
      
    
      async store ({ request }) {
        const data = request.only([
          'nome'
        ])
    
        const tipoProduto = await TipoProduto.create(data)
    
        return tipoProduto
      }
    
    
      async show ({ params }) {
        const tipoProduto = await TipoProduto.findOrFail(params.id)
    
        return tipoProduto
      }
    
      
      async update ({ params, request }) {
        const tipoProduto = await TipoProduto.findOrFail(params.id)
    
        const data = request.only([
          'nome'
        ])
    
        tipoProduto.merge(data)
    
        await tipoProduto.save()
    
        return tipoProduto
      }
    
      
      async destroy ({ params, response }) {
        const tipoProduto = await TipoProduto.find(params.id)
    
        if (tipoProduto != null){
          await tipoProduto.delete()
          return response.send({ok: 'Tipo do produto deletado com sucesso'})
        } else{
          return response.send({error: 'Tipo do produto não encontrado'})
        }
      }
}

module.exports = TipoProdutoController
