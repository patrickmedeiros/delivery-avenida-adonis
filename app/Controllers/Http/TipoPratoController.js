'use strict'

const TipoPrato = use('App/Models/TipoPrato')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class TipoPratoController {
    async index () {
        const tiposPratos = TipoPrato.all()
    
        return tiposPratos
      }
      
    
      async store ({ request }) {
        const data = request.only([
          'nome',
          'tipo_produto_id',
          'item_id'
        ])
    
        const tipoPrato = await TipoPrato.create(data)
    
        return tipoPrato
      }
    
    
      async show ({ params }) {
        const tipoPrato = await TipoPrato.findOrFail(params.id)
    
        return tipoPrato
      }
    
      
      async update ({ params, request }) {
        const tipoPrato = await TipoPrato.findOrFail(params.id)
    
        const data = request.only([
          'nome',
          'tipo_produto_id',
          'item_id'
        ])
    
        tipoPrato.merge(data)
    
        await tipoPrato.save()
    
        return tipoPrato
      }
    
      
      async destroy ({ params, response }) {
        const tipoPrato = await TipoPrato.find(params.id)
    
        if (tipoPrato != null){
          await tipoPrato.delete()
          return response.send({ok: 'Tipo do prato deletado com sucesso'})
        } else{
          return response.send({error: 'Tipo do prato não encontrado'})
        }
      }
}

module.exports = TipoPratoController
