'use strict'

const Prato = use('App/Models/Prato')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pratoes
 */
class PratoController {
  async index () {
    const pratos = Prato.all()

    return pratos
  }
  

  async store ({ request }) {
    const data = request.only([
      'nome',
      'valor',
      'descricao',
      'tipo_prato_id',
      'item_id'
    ])

    const prato = await Prato.create(data)

    return prato
  }


  async show ({ params }) {
    const prato = await Prato.findOrFail(params.id)

    return prato
  }

  
  async update ({ params, request }) {
    const prato = await Prato.findOrFail(params.id)

    const data = request.only([
      'nome',
      'valor',
      'descricao',
      'tipo_prato_id',
      'item_id'
    ])

    prato.merge(data)

    await prato.save()

    return prato
  }

  
  async destroy ({ params, response }) {
    const prato = await Prato.find(params.id)

    if (prato != null){
      await prato.delete()
      return response.send({ok: 'Prato deletado com sucesso'})
    } else{
      return response.send({error: 'Prato não encontrado'})
    }
  }
}

module.exports = PratoController
