'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Item extends Model {

    prato(){
        return this.belongsTo('App/Models/Prato')
    }

    tipoPrato(){
        return this.belongsTo('App/Models/TipoPrato')
    }

}

module.exports = Item
