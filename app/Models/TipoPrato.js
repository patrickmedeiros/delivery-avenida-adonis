'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TipoPrato extends Model {

    
    pratos(){
        return this.hasMany('App/Models/Prato')
    }

    itens(){
        return this.hasMany('App/Models/Item')
    }
    

}

module.exports = TipoPrato
