'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Prato extends Model {

    
    pedido(){
        return this.belongsTo('App/Models/Pedido')
    }

    itens(){
        return this.hasMany('App/Models/Item')
    }

    tipoPrato(){
        return this.belongsTo('App/Models/TipoPrato')
    }
    
}

module.exports = Prato
