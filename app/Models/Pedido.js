'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pedido extends Model {
    
    usuario(){
        return this.belongsTo('App/Models/Usuario')
    }

    pratos(){
        return this.hasMany('App/Models/Prato')
    }
    
}

module.exports = Pedido
