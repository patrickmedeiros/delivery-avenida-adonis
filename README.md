# 1. Delivery

API Delivery-Avenida

Create database and test database

Create file .env (examples in .env-example)

Install dependencies:
 $ npm install

Migrate and seed database
  $ adonis seed

Start App
 $ npm start
# 2. Documentation
## 2.1. Create new usuarios
    POST /usuarios
<h2>You have to send a body containing
<p>{   
  <p>"username": "teste",</p>
  <p>"email": "teste@avenida.com",
    <p>"password": "192837",
	<p>"admin": "1",
	<p>"cpf":"123456"
<p>}

<h2>Return new usuarios created

<p>{   
  <p>"username": "teste",
  <p>"email": "teste@avenida.com",
  <p>"password": "$2a$10$tJr29HLwXwUW2LCcx3DXf.m2/TDXSckNGmjFQ/o59FVx4sLZsSjoG",
  <p>"admin": "1",
  <p>"cpf": "123456",
  <p>"created_at": "2019-07-10 12:08:41",
  <p>"updated_at": "2019-07-10 12:08:41",
  <p>"id": 2
<p>}


## 2.2. Authenticate usuarios
    POST /authenticate
<h2>You have to send a body containing

<p>{
	<p>"email":"admin@avenida.com",
	<p>"password": "teste",
	
<p>}
<h2>Returns logged usuarios
<p>{
  <p>"email": "admin@avenida.com",
  <p>"id": "6",
  <p>"username": "admin",
  <p>"accesstoken": 
  <p>{
    <p>"type": "bearer",
    <p>"token": <p>"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjYsImlhdCI6MTU2Mjg4Mjk1M30.K8pb0vvx6LLwope5f1AhXRVmQLoJ8xy2WcPfsZIsdck",
    <p>"refreshToken": null
  <p>}
<p>}

<h2>For protected routes For protected routes, send a header containing your token, example:
<p>{ 
   <p> "Autorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjEsImlhdCI6MTU2MTQzMzEzMH0.56-bOIx_eyd5QoHysPkxQ2mWX1tHZiZN1v1jW5NTFrs"
<p>}

## 2.3. List usuarios List all usuarios, protected route
    GET /usuarios
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Returns all usuarios
<p>[
  <p>{
   <p> "id": 1,
    <p>"cpf": "99999999999",
   <p> "username": "admin",
    <p>"email": "admin@avenida.com",
    <p>"password": "$2a$10$hkN.E4ugEw/UhRPCFf.oWewLrGdlOznWwfc8XJq/orWWPTiRX8pmO",
    <p>"admin": 1,
    <p>"created_at": "2019-07-03 19:54:25",
    <p>"updated_at": "2019-07-03 19:54:25"
  <p>},
  <p>{
    <p>"id": 2,
    <p>"cpf": "123456",
    <p>"username": "teste",
    <p>"email": "teste@avenida.com",
    <p>"password": "$2a$10$tJr29HLwXwUW2LCcx3DXf.m2/TDXSckNGmjFQ/o59FVx4sLZsSjoG",
    <p>"admin": 1,
    <p>"created_at": "2019-07-10 12:08:41",
    <p>"updated_at": "2019-07-10 12:08:41"
  <p>}
<p>]

## 2.4. List One usuarios ID List one usuarios, protected route
    GET /usuarios/:id
    example
    GET /usuarios/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Returns usuarios
<p>{
  <p>"id": 1,
  <p>"cpf": "99999999999",
  <p>"username": "admin",
  <p>"email": "admin@avenida.com",
  <p>"password": "$2a$10$hkN.E4ugEw/UhRPCFf.oWewLrGdlOznWwfc8XJq/orWWPTiRX8pmO",
  <p>"admin": 1,
  <p>"created_at": "2019-07-03 19:54:25",
  <p>"updated_at": "2019-07-03 19:54:25"
<p>}

## 2.5. Update usuarios
    PUT /usuarios/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>You have to send a body containing
<p>{
    <p>"username": "patrick1",
    <p>"email": "patrick@avenida.com",
    <p>"password": "192837"
  <p>}
<h2>Returns usuarios
<p>{
    <p>"id": 1,
    <p>"username": "patrick1",
    <p>"email": "patrick@avenida.com",
    <p>"password": "192837"
<p>}

## 2.6. Delete usuarios
    DELETE /usuarios/2
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Result
<p>{
    <p>"code": "200",
    <p>"message": " deletado com sucesso"
<p>}

## 2.7. Create new itens
    POST /itens
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<p>{
	<p>"nome": "linguica",
    	<p>"quantidade": 5,
   	<p>"preco": 5,
    	<p>"descricao": "linguica toscana",
	<p>"tipo_produto_id": 2
  
<p>}
<h2>Return new itens created
<p>{
  <p>"nome": "linguica",
  <p>"quantidade": 5,
  <p>"preco": 5,
  <p>"descricao": "linguica toscana",
  <p>"tipo_produto_id": 2,
  <p>"created_at": "2019-07-10 14:58:35",
  <p>"updated_at": "2019-07-10 14:58:35",
  <p>"id": 5
<p>}

## 2.8. List itens List all itens, protected route
    GET /itens
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}

<h2>Returns all itens
<p>[
<p>{
    <p>"id": 1,
    <p>"nome": "arroz",
    <p>"quantidade": 5,
    <p>"preco": 3,
    <p>"descricao": "arroz",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_produto_id": 3
  <p>},
  <p>{
    <p>"id": 2,
    <p>"nome": "feijao",
    <p>"quantidade": 6,
    <p>"preco": 8,
    <p>"descricao": "feijao",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_produto_id": null
  <p>},
  <p>{
    <p>"id": 3,
    <p>"nome": "massa",
    <p>"quantidade": 3,
    <p>"preco": 6,
    <p>"descricao": "massa",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_produto_id": 3
  <p>},
  <p>{
    <p>"id": 4,
    <p>"nome": "descricao",
    <p>"quantidade": 7,
    <p>"preco": 10,
    <p>"descricao": "abobora",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_produto_id": 2
  <p>},
  <p>{
    <p>"id": 5,
    <p>"nome": "linguica",
    <p>"quantidade": 5,
    <p>"preco": 5,
    <p>"descricao": "linguica toscana",
    <p>"created_at": "2019-07-10 14:58:35",
    <p>"updated_at": "2019-07-10 14:58:35",
    <p>"tipo_produto_id": 2
  <p>}
<p>]

## 2.9. List One itens ID List one itens, protected route
    GET /itens/:id
    example
    GET /itens/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Returns itens
<p>{
  <p>"id": 1,
  <p>"nome": "arroz",
  <p>"quantidade": 5,
  <p>"preco": 3,
  <p>"descricao": "arroz",
  <p>"created_at": "2019-07-03 19:54:24",
  <p>"updated_at": "2019-07-03 19:54:24",
  <p>"tipo_produto_id": 1
<p>}
## 2.10. Update itens
    PUT /itens/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>You have to send a body containing
<p>{
    <p>"nome": "linguica2",
    <p>"quantidade": 5,
    <p>"preco": 5,
    <p>"descricao": "linguica toscana",
		<p>"tipo_produto_id": 2
  <p>}  
<h2>Returns itens
<p>{ 
  <p>"id": 2,
  <p>"nome": "linguica2",
  <p>"quantidade": 5,
  <p>"preco": 5,
  <p>"descricao": "linguica toscana",
  <p>"created_at": "2019-07-03 19:54:24",
  <p>"updated_at": "2019-07-10 15:06:33",
  <p>"tipo_produto_id": 2
<p>}

## 2.11. Delete itens
    DELETE /itens/2
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Result
<p>{
    <p>"code": "200",
    <p>"message": " deletado com sucesso"
<p>}

## 2.12. Create new pratos
    POST /pratos
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<p>{
    <p>"nome": "Risoto",
    <p>"valor": 22,
    <p>"descricao": "Risoto vegetariano especial do chef",
    <p>"tipo_prato_id": 1,
    <p>"item_id": 5
 <p>}
<h2>Return new pratos created
<p>{
  <p>"nome": "Risoto",
  <p>"valor": 22,
  <p>"descricao": "Risoto vegetariano especial do chef",
  <p>"tipo_prato_id": 1,
  <p>"item_id": 5,
  <p>"created_at": "2019-07-10 15:14:22",
  <p>"updated_at": "2019-07-10 15:14:22",
  <p>"id": 7
<p>}
## 2.13. List pratos List all pratos, protected route
    GET /pratos
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}

<h2>Returns all pratos
<p>[
  <p>{
    <p>"id": 1,
    <p>"nome": "Risoto de Batata Inglesa",
    <p>"valor": 14,
    <p>"descricao": "Risoto vegetariano especial do chef",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_prato_id": 1,
    <p>"item_id": 2
  <p>},
  <p>{
    <p>"id": 2,
    <p>"nome": "Costela ao molho madeira",
    <p>"valor": 19.9,
    <p>"descricao": "200g de costela ao molho madeira, que acompanha arroz branco e salada verde",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_prato_id": 2,
    <p>"item_id": 2
  <p>},
  <p>{
    <p>"id": 3,
    <p>"nome": "Massa ao molho de brócolis",
    <p>"valor": 12.5,
    <p>"descricao": "250g de massa tipo parafuso ao molho de brócolis, com acompanhamento de salada verde",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_prato_id": 1,
    <p>"item_id": 1
  <p>},
  <p>{
    <p>"id": 4,
    <p>"nome": "Massa ao molho bolonhesa",
    <p>"valor": 12.5,
    <p>"descricao": "250g de massa tipo parafuso ao molho de bolonhesa, com acompanhamento de salada verde",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_prato_id": 3,
    <p>"item_id": 2
  <p>},
  <p>{
    <p>"id": 5,
    <p>"nome": "Galinhada Caipira",
    <p>"valor": 16.5,
    <p>"descricao": "300g arroz com galinha caipira, com acompanhamento de salada campeira",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_prato_id": 3,
    <p>"item_id": 3
  <p>},
  <p>{
    <p>"id": 6,
    <p>"nome": "Strogonoff de carne",
    <p>"valor": 16.5,
    <p>"descricao": "180g de strogonoff de carne, com acompanhamento de salada verde, arroz e batata palha",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24",
    <p>"tipo_prato_id": 2,
    <p>"item_id": 1
  <p>},
  <p>{
    <p>"id": 7,
    <p>"nome": "Risoto",
    <p>"valor": 22,
    <p>"descricao": "Risoto vegetariano especial do chef",
    <p>"created_at": "2019-07-10 15:14:22",
    <p>"updated_at": "2019-07-10 15:14:22",
    <p>"tipo_prato_id": 1,
    <p>"item_id": 5
  <p>}
<p>]

## 2.14. List One pratos ID List one usuarios, protected route
    GET /pratos/:id
    example
    GET /pratos/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Returns pratos
<p>{
 <p> "id": 1,
  <p>"nome": "Risoto de Batata Inglesa",
  <p>"valor": 14,
  <p>"descricao": "Risoto vegetariano especial do chef",
  <p>"created_at": "2019-07-03 19:54:24",
  <p>"updated_at": "2019-07-03 19:54:24",
  <p>"tipo_prato_id": 1,
  <p>"item_id": 2
<p>}

## 2.15. Update pratos
    PUT /pratos/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>You have to send a body containing
<p>{
    	<p>"nome": "Bife welington1",
    	<p>"valor": 15,
    	<p>"descricao": "Bife welington",
	<p>"tipo_prato_id": 2,
	<p>"item_id": 4
 <p>}  
<h2>Returns pratos
<p>{
  <p>"id": 1,
  <p>"nome": "Bife welington1",
  <p>"valor": 15,
  <p>"descricao": "Bife welington",
  <p>"created_at": "2019-07-03 19:54:24",
  <p>"updated_at": "2019-07-10 15:20:54",
  <p>"tipo_prato_id": 2,
  <p>"item_id": 4
<p>}

## 2.16. Delete pratos
    DELETE /pratos/2
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Result
<p>{
    <p>"code": "200",
    <p>"message": " deletado com sucesso"
<p>}

## 2.17. Create new produtos
    POST /produtos
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<p>{
    <p>"nome": "vegetal"
<p>}
<h2>Return new produtos created
<p>{
  <p>"nome": "vegetal",
  <p>"created_at": "2019-07-10 15:41:02",
  <p>"updated_at": "2019-07-10 15:41:02",
  <p>"id": 3
<p>}
## 2.18. List produtos List all produtos, protected route
    GET /produtos
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}

<h2>Returns all produtos
<p>[
  <p>{
    <p>"id": 1,
    <p>"nome": "proteina",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24"
  <p>},
  <p>{
    <p>"id": 2,
    <p>"nome": "acompanhamento",
    <p>"created_at": "2019-07-03 19:54:24",
    <p>"updated_at": "2019-07-03 19:54:24"
  <p>},
  <p>{
    <p>"id": 3,
    <p>"nome": "vegetal",
    <p>"created_at": "2019-07-10 15:41:02",
    <p>"updated_at": "2019-07-10 15:41:02"
  <p>}
<p>]

## 2.19. List One produtos ID List one produtos, protected route
    GET /produtos/:id
    example
    GET /produtos/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>Returns produtos
<p>{
  <p>"id": 1,
  <p>"nome": "proteina",
  <p>"created_at": "2019-07-03 19:54:24",
  <p>"updated_at": "2019-07-03 19:54:24"
<p>}

## 2.20. Update Produtos
    PUT /produtos/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" 
<p>}
<h2>You have to send a body containing
<p>{
    <p>"nome": "vegetariano"
<p>}  
<h2>Returns produtos
<p>{
  <p>"id": 3,
  <p>"nome": "vegetariano",</p>
  <p>"created_at": "2019-07-10 15:41:02",</p>
  <p>"updated_at": "2019-07-10 15:47:16"</p>
<p>}

## 2.21.Delete Produtos
    DELETE /produtos/2
<h2>You have to send a header containing</p>
<p>{   
    <p>"Autorization": "Bearer {your token}" </p>
<p>}
<h2>Result
<p>{
    <p>"code": "200",</p>
    <p>"message": " deletado com sucesso"</p>
<p>}

## 2.22.Create new pedidos
    POST /pedidos
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" </p>
<p>}
<p>{
    <p>"nome": "vegetal"</p>
<p>}
<h2>Return new pedidos created
<p>{
	<p>"valor": 32,</p>
	<p>"pago": 1,</p>
	<p>"user_id": 1,</p>
	<p>"prato_id": 3</p>
<p>}
## 2.23.List pedidos List all pedidos, protected route
    GET /pedidos
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" </p>
<p>}

<h2>Returns all pedidos
<p>[
  <p>{
    <p>"id": 1,</p>
    <p>"valor": 14,</p>
    <p>"pago": 1,</p>
    <p>"created_at": "2019-07-03 19:54:24",</p>
    <p>"updated_at": "2019-07-03 19:54:24",</p>
    <p>"user_id": 2,</p>
    <p>"prato_id": 1</p>
  <p>},
  <p>{
    <p>"id": 2,</p>
    <p>"valor": 16,</p>
    <p>"pago": 0,</p>
    <p>"created_at": "2019-07-03 19:54:24",</p>
    <p>"updated_at": "2019-07-03 19:54:24",</p>
    <p>"user_id": 1,</p>
    <p>"prato_id": 2</p>
  <p>},
  <p>{
    <p>"id": 3,</p>
    <p>"valor": 22,</p>
    <p>"pago": 0,</p>
    <p>"created_at": "2019-07-03 19:54:24",</p>
    <p>"updated_at": "2019-07-03 19:54:24",</p>
    <p>"user_id": 2,</p>
    <p>"prato_id": 2</p>
  <p>},
  <p>{
    <p>"id": 4,</p>
    <p>"valor": 40,</p>
    <p>"pago": 1,</p>
    <p>"created_at": "2019-07-03 19:54:24",</p>
    <p>"updated_at": "2019-07-03 19:54:24",</p>
    <p>"user_id": 3,</p>
    <p>"prato_id": 1</p>
  <p>}
<p>]

## 2.24.List One pedidos ID List one pedidos, protected route
    GET /pedidos/:id
    example
    GET /pedidos/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" </p>
<p>}
<h2>Returns pedidos
<p>{
  <p>"id": 1,</p>
  <p>"valor": 14,</p>
  <p>"pago": 1,</p>
  <p>"created_at": "2019-07-03 19:54:24",</p>
  <p>"updated_at": "2019-07-03 19:54:24",</p>
  <p>"user_id": 1,</p>
  <p>"prato_id": 2</p>
<p>}

## 2.25.Update pedidos
    PUT /pedidos/1
<h2>You have to send a header containing
<p>{   
    <p>"Autorization": "Bearer {your token}" </p>
<p>}
<h2>You have to send a body containing
<p>{
    <p>"valor": 20</p>
  <p>}  
<h2>Returns pedidos
<p>{
  <p>"id": 1,</p>
  <p>"valor": 20,</p>
  <p>"pago": 1,</p>
  <p>"created_at": "2019-07-03 19:54:24",</p>
  <p>"updated_at": "2019-07-10 15:56:56",</p>
  <p>"user_id": 1,</p>
  <p>"prato_id": 2</p>
<p>}


## 2.26.Delete pedidos
    DELETE /pedidos/2
<h2>You have to send a header containing

<p>{   
    <p>"Autorization": "Bearer {your token}"</p> 
<p>}

<h2>Result

<p>{

    <p>"code": "200",</p>
    <p>"message": " deletado com sucesso"</p>
<p>}