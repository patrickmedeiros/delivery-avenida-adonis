'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColumnCpfAndAdminUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('cpf', 11).after('id')
      table.boolean('admin').after('password')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('cpf')
      table.dropColumn('admin')
    })
  }
}

module.exports = AddColumnCpfAndAdminUsersSchema
