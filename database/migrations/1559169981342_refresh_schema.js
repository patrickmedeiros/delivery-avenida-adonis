'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RefreshSchema extends Schema {
  up () {
    this.create('refreshes', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('refreshes')
  }
}

module.exports = RefreshSchema
