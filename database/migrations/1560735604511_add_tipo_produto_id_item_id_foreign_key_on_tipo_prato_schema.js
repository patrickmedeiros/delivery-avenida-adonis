'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddTipoProdutoIdItemIdForeignKeyOnTipoPratoSchema extends Schema {
  up () {
    this.table('tipo_pratoes', (table) => {
      table
        .integer('tipo_produto_id')
        .unsigned()
        .references('id')
        .inTable('tipo_produtos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('item_id')
        .unsigned()
        .references('id')
        .inTable('items')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
      this.table('tipo_pratoes', (table) => {
      table.dropColumn('tipo_produto_id')
      table.dropColumn('item_id')
    })
  }
}

module.exports = AddTipoProdutoIdItemIdForeignKeyOnTipoPratoSchema
