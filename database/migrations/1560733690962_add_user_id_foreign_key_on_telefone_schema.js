'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserIdForeignKeyOnTelefoneSchema extends Schema {
  up () {
    this.table('telefones', (table) => {
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
      this.table('telefones', (table) => {
      table.dropColumn('user_id')
    })
  }
}

module.exports = AddUserIdForeignKeyOnTelefoneSchema
