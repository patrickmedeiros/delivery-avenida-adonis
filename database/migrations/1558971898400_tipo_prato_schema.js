'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoPratoSchema extends Schema {
  up () {
    this.create('tipo_pratoes', (table) => {
      table.increments()
      table.string('nome')      
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_pratoes')
  }
}

module.exports = TipoPratoSchema
