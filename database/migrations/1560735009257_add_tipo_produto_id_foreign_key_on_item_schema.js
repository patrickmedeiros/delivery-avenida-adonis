'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddTipoProdutoIdForeignKeyOnItemSchema extends Schema {
  up () {
    this.table('items', (table) => {
      table
        .integer('tipo_produto_id')
        .unsigned()
        .references('id')
        .inTable('tipo_produtos')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
      this.table('items', (table) => {
      table.dropColumn('tipo_produto_id')
    })
  }
    
}

module.exports = AddTipoProdutoIdForeignKeyOnItemSchema
