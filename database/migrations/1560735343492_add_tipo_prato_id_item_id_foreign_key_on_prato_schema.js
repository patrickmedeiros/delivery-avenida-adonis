'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddTipoPratoIdItemIdForeignKeyOnPratoSchema extends Schema {
  up () {
    this.table('pratoes', (table) => {
      table
        .integer('tipo_prato_id')
        .unsigned()
        .references('id')
        .inTable('tipo_pratoes')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('item_id')
        .unsigned()
        .references('id')
        .inTable('items')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
      this.table('pratoes', (table) => {
      table.dropColumn('tipo_prato_id')
      table.dropColumn('item_id')
    })
  }
}

module.exports = AddTipoPratoIdItemIdForeignKeyOnPratoSchema
