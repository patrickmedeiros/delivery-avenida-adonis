'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PratoSchema extends Schema {
  up () {
    this.create('pratoes', (table) => {
      table.increments()
      table.string('nome')
      table.double('valor', 9 ,2)
      table.string('descricao')
      table.timestamps()
    })
  }

  down () {
    this.drop('pratoes')
  }
}

module.exports = PratoSchema
