'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersTelefonesSchema extends Schema {
  up () {
    this.create('users_telefones', (table) => {
      table.integer('user_id').unsigned().index('user_id')
      table.integer('telefone_id').unsigned().index('telefone_id')

      table.foreign('user_id').references('users.id').onDelete('cascade')
      table.foreign('telefone_id').references('telefones.id').onDelete('cascade')

    })
  }

  down () {
    this.drop('users_telefones')
  }
}

module.exports = UsersTelefonesSchema
