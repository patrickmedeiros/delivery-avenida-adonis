'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserIdPratoIdForeignKeyOnPedidoSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('prato_id')
        .unsigned()
        .references('id')
        .inTable('pratoes')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
      this.table('pedidos', (table) => {
      table.dropColumn('user_id')
      table.dropColumn('prato_id')
    })
  }
}

module.exports = AddUserIdPratoIdForeignKeyOnPedidoSchema
