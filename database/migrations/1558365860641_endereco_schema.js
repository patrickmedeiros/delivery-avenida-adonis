'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EnderecoSchema extends Schema {
  up () {
    this.create('enderecos', (table) => {
      table.increments()
      table.string('logradouro')
      table.integer('numero')
      table.string('complemento')
      table.integer('cep')
      table.integer('tipo')
      /** esta classe tem que criar uma outra tabela que seria a endereco_usuario que iráter duas chaves estrangeiras
       * o id do endereco e o id do usuario
        */
      table.timestamps()
    })
  }

  down () {
    this.drop('enderecos')
  }
}

module.exports = EnderecoSchema
