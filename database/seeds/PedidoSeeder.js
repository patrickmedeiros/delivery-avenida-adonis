'use strict'

/*
|--------------------------------------------------------------------------
| PedidoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class PedidoSeeder {
  async run () {
    await Database.table('pedidos').insert([
      {'valor': 14, 'pago': 1, created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'valor': 16, 'pago': 0, created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'valor': 22, 'pago': 0, created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'valor': 40, 'pago': 1, created_at: Database.fn.now(), updated_at: Database.fn.now()},
    ])
  }
}

module.exports = PedidoSeeder
