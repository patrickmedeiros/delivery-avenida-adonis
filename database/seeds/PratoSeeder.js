'use strict'

/*
|--------------------------------------------------------------------------
| PratoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class PratoSeeder {
  async run () {
    await Database.table('pratoes').insert([
      {'nome': 'Risoto de Batata Inglesa', 'valor': 14, 'descricao': 'Risoto vegetariano especial do chef', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'Costela ao molho madeira','valor': 19.90, 'descricao': '200g de costela ao molho madeira, que acompanha arroz branco e salada verde', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'Massa ao molho de brócolis','valor': 12.50, 'descricao': '250g de massa tipo parafuso ao molho de brócolis, com acompanhamento de salada verde', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'Massa ao molho bolonhesa','valor': 12.50, 'descricao': '250g de massa tipo parafuso ao molho de bolonhesa, com acompanhamento de salada verde', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'Galinhada Caipira','valor': 16.50, 'descricao': '300g arroz com galinha caipira, com acompanhamento de salada campeira', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'Strogonoff de carne','valor': 16.50, 'descricao': '180g de strogonoff de carne, com acompanhamento de salada verde, arroz e batata palha', created_at: Database.fn.now(), updated_at: Database.fn.now()},
    ])
  }
}

module.exports = PratoSeeder
