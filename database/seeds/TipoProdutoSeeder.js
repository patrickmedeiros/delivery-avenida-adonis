'use strict'

/*
|--------------------------------------------------------------------------
| TipoProdutoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class TipoProdutoSeeder {
  async run () {
    await Database.table('tipo_produtos').insert([
      {'nome': 'proteina', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'acompanhamento', created_at: Database.fn.now(), updated_at: Database.fn.now()}
    ])
  }
}

module.exports = TipoProdutoSeeder
