'use strict'

/*
|--------------------------------------------------------------------------
| TipoPratoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class TipoPratoSeeder {
  async run () {
    await Database.table('tipo_pratoes').insert([
      {'nome': 'carnívoro', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'vegetariano', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'vegano', created_at: Database.fn.now(), updated_at: Database.fn.now()},
    ])
  }
}

module.exports = TipoPratoSeeder
