'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
const Hash = use('Hash')
const Database = use('Database')

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
class UserSeeder {
  async run () {
    await Database.table('users').insert([
      { cpf: '99999999999', username: 'admin', email: 'admin@avenida.com',  password: await Hash.make('teste'), admin: 1, created_at: Database.fn.now(), updated_at: Database.fn.now()},
      { cpf: '11111111111', username: 'consumer', email: 'consumer@avenida.com', password: await Hash.make('teste'), admin: 0, created_at: Database.fn.now(), updated_at: Database.fn.now()}
    ])
  }
}

module.exports = UserSeeder