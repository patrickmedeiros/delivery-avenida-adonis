'use strict'

/*
|--------------------------------------------------------------------------
| ItemSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')


class ItemSeeder {
  async run () {
    await Database.table('items').insert([
      {'nome': 'arroz', 'quantidade': 5, 'preco': 3, 'descricao': 'arroz', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'feijao', 'quantidade': 6, 'preco': 8, 'descricao': 'feijao', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'massa', 'quantidade': 3, 'preco': 6, 'descricao': 'massa', created_at: Database.fn.now(), updated_at: Database.fn.now()},
      {'nome': 'descricao', 'quantidade': 7, 'preco': 10, 'descricao': 'abobora', created_at: Database.fn.now(), updated_at: Database.fn.now()},
    ])
  }
}

module.exports = ItemSeeder
